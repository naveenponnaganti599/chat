var express = require('express');
var app = express();
var mongodb = require('./mongodb.js');

global.__root = __dirname + '/';

var UserController = require(__root + 'user/UserController');
app.use('/api/users', UserController);

var AuthController = require(__root + 'auth/AuthController');
app.use('/api/auth', AuthController);

var testController = require(__root + 'test/TestController');
app.use('/api', testController);

var server = require('http').Server(app);

module.exports = server ;