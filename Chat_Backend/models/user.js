var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
    FirstName: { type: String, required: true },
    LastName: { type: String, required: true },
    UserName: { type: String, required: true },
    PhoneNumber: { type: String, required: true },
    Password : {type:String,required : true},
    Email: { type: String, required: true },
    JoinedAt: { type: Date, required: true },
    Devices: { type: Array, required: false },
    Blocked: { type: Array, required: false },
    Channels: { type: Array, required: false },
    Primary: { type: String, required: true },
},{collection : 'Users'});


module.exports = mongoose.model('User', UserSchema);