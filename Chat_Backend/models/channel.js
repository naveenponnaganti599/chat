var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ChannelSchema = new Schema({
    ChannelType: { type: String, required: true },
    ChannelCreatedAt: { type: Date, required: true },
    ChannelAdmin: { type: Array, required: true },
    ChannelUsers: { type: Array, required: true }
},{collection : 'Channels'});


module.exports = mongoose.model('Channel', ChannelSchema);