var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MessageSchema = new Schema({
    MessageCreatedAt: { type: Date, required: true },
    MessageText: { type: String, required: true },
    ChannelUsers: { type: Array, required: true },
    ChannelAdmin: { type: Array, required: true },
    ChannelId: {type: String, required: true},
    ReadBy: { type: Array, required: false },
    DeliveredTo: { type: Array, required: false },
    Sender: { type: Object, required: true }
},{collection : 'Messages'});


module.exports = mongoose.model('Message', MessageSchema);