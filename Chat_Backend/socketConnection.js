var jwt = require('jsonwebtoken');
var config = require('./config');
var action = require('./action.js');
var io = require('./socketEmissions.js').io;

io.use(function (socket, next) {
    if (socket.handshake.query && socket.handshake.query.token) {
        jwt.verify(socket.handshake.query.token, config.secret, function (err, decoded) {
            if (err) {
                console.log(err);
                return next(new Error('Authentication error'));
            }

            socket.decoded = decoded.user;
            next();
        });
    } else {
        next(new Error('Authentication error'));
    }
})
    .on('connection', async function (socket) {
        action.connectionAction(socket);
        socket.on('disconnect', function () {
            action.disconnetionAction(socket);
        })
        .on('RequestForFriendsList', async function(username){
            action.usersFriendList(socket, username);
        })
            // .on('ChannelMessage', async function (obj) {
            //     action.messagetoExistingChannel(socket, obj);
            // })
            // .on('RequestForFriendsList', async function(obj){
            //     // why would client request for friends list.
            //     //if requested what should he send in params.
            //     //we need to get his contacts to check whether other user is registered with the application.
            // })
            // .on('ChannelMessage', async function(obj){
            //     //Emitted at the client end but not registered in the server.
            // })
            // .on('CheckRegistredUsers', async function(obj){
            //     //Emitted at the client end but not registered in the server.
            // })

            .on("CheckRegisteredUsers", async function(arrayOfNumbers){
                 action.checkExistingUsers(socket, arrayOfNumbers);
            })
            .on('ChannelMessage', async function (obj) {
                action.messagetoExistingChannel(socket, obj);
            })
            .on('CreateNewChannel', async function (obj) {
                action.createNewChannel(socket, obj);
            })
    });




