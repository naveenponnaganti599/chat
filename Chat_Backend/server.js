var app = require('./app');
var port = process.env.PORT || 3000;
var socketConnection = require('./socketConnection.js');

app.listen(port, function() {
  console.log('Express server listening on port ' + port);
});
