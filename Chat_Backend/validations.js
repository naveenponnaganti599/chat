var mongoQuery = require('./mongoDBQueries');

var userAvailable = async function (data) {
    var user = await mongoQuery.userNameCheck(data);
    return user;
}

var channelAvailable = async function () {
    var channel = await mongoQuery.channelCheck(data);
    return channel;
}



module.exports = {
    userAvailable,
    channelAvailable,
}