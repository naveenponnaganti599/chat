var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

router.get('/test', function (req, res) {
    res.status(200).send('api is working');
 });

 module.exports = router;