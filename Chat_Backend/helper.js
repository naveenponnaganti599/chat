var validation = require('./validations.js');
var mongoQuery = require('./mongoDBQueries.js');
var sqlQuery = require('./sqlDBQueries.js');

var createChannelDocument = function (arrayOfUsers, obj) {
    var channelUsersArray = getChannelUsers(arrayOfUsers);
    var admin = arrayOfUsers.find(item => {
        return item.Primary === obj.From
    });

    var channelObject = {
        "ChannelType": "private",
        "ChannelCreatedAt": new Date(),
        "ChannelAdmin": [
            {
                "UserName": admin.Primary,
                "UniqueIdentifier": admin._id
            }
        ],
        "ChannelUsers": channelUsersArray
    }
    return channelObject;
}

var createruserDocument = function (arrayOfUsers, obj) {
    var arrOfChannelUsers = [];
    arrayOfUsers.forEach(element => {
        var item = {
            "PhoneNumber ": element.PhoneNumber, 
            "UserName": element.Primary,
            "UserId": element._id,
            "Email": element.Email,
            "ChannelType" :obj.ChannelType,
            "ChannelId" :obj._id,
        }
        arrOfChannelUsers.push(item);
    });
    return arrOfChannelUsers
}

function getChannelUsers(arrayOfUsers) {
    var arrayOfChannelUsers = [];

    arrayOfUsers.forEach(element => {
        var item = {
            "UserName": element.Primary,
            "UniqueIdentifier": element._id
        }
        arrayOfChannelUsers.push(item);
    });

    return arrayOfChannelUsers;
}

var getFriendslist = async function (username) {

    var arrayOfFriendsList = [];
    return await mongoQuery.CheckFriends(username).then((items) => asyncForEach(items
        , async (element) => {
            friendsListObject = {
                "Number": element.PhoneNumber,
                "Email": element.Email,
                "ChannelId": await mongoQuery.GetFriendsChannels(username, element.PhoneNumber).then((id) => { return id[0]['_id'] }).catch((err) => { console.log(err); })
            }
            arrayOfFriendsList.push(friendsListObject);
        })).then(() => { return arrayOfFriendsList }); ``
}

async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
}

var getUserforChat = async function (data) {
    return validation.userAvailable(
        { $or: [{ Primary: data.To }, { Primary: data.From }] }
    )
        .then((user) => {
            return user;
        }).catch((err) => {
            console.log('error : ', err);
        });
}

var userCheck = async function (data) {
    return validation.userAvailable(
        { $or: [{ Primary: data.To }, { Primary: data.From }] }
    )
        .then((user) => {
            return user;
        }).catch((err) => {
            console.log('error : ', err);
        });
}

var getRegisteredUsers = async function (data) {

    return await mongoQuery.getRegisteredUsers(
        { $or: data },{Primary : 1, _id : 0}
    )
        .then((usersArray) => {
            console.log('usersArray',usersArray);
            return usersArray;
        }).catch((err) => {
            console.log('error : ', err);
        });
}

var createChannel = function (data) {
    return mongoQuery.insertChannel(data)
        .then((result) => {
            return result;
        }).catch((err) => {
            console.log(err);
        });
}

var createMessageDocument = function (channelDocument, users, message, senderPhoneNumber) {

    var sender = users.find(item => {
        return item.Primary === senderPhoneNumber
    });

    var senderObj = {
        "UserName": sender.Primary,
        "UniqueIdentifier": sender._id
    }

    var messageObject = {
        "MessageCreatedAt": new Date(),
        "MessageText": message,
        "ChannelUsers": channelDocument.ChannelUsers,
        "ChannelAdmin": channelDocument.ChannelAdmin,
        "ChannelId": channelDocument._id,
        "ReadBy": [],
        "DeliveredTo": [],
        "Sender": senderObj
    }

    return messageObject;
}

var createMessage = function (data) {
    return mongoQuery.insertMessage(data)
        .then((result) => {
            return result;
        }).catch((err) => {
            console.log(err);
        });
}

var getSocket = function (userId) {
    return sqlQuery.getSocket(userId)
        .then((result) => {
            return result;
        }).catch((err) => {
            console.log(err);
        });
}

var updateSocket = function (data) {
    return sqlQuery.updateSocket(data)
        .then((result) => {
            return result;
        }).catch((err) => {
            console.log(err);
        });
}

var insertSocket = function (data) {
    return sqlQuery.insertSocket(data)
        .then((result) => {
            return result;
        }).catch((err) => {
            console.log(err);
        });
}

var deleteSocket = function (data) {
    return sqlQuery.removeSocket(data)
        .then((result) => {
            return result;
        }).catch((err) => {
            console.log(err);
        });
}

var deleteUserSocket = function (data) {
    return sqlQuery.removeUserSocket(data)
        .then((result) => {
            return result;
        }).catch((err) => {
            console.log(err);
        });
}


module.exports = {
    createChannelDocument,
    getFriendslist,
    getUserforChat,
    userCheck,
    createChannel,
    createMessageDocument,
    createMessage,
    getSocket,
    insertSocket,
    updateSocket,
    deleteSocket,
    deleteUserSocket,
    userCheck,
    getRegisteredUsers,
    createruserDocument
}