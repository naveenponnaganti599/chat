const sql = require("msnodesqlv8");

const connectionString = "server=ggku2ser6 ;Database= {Chat}; Trusted_Connection=Yes; Driver={SQL Server Native Client 11.0}";

var sqlConnectionObject = async function () {

    return new Promise(async(resolve, reject) => {

      await sql.open(connectionString, function (err, con) {
            if (err) {
                if (Array.isArray(err)) {
                    err.forEach((e) => {
                        console.log(e.message);
                    });
                    resolve(null);
                } else {
                    console.log(err.message);
                    resolve(null);
                }
            } else {
                resolve(con);
            }
        });
    });
};

module.exports = sqlConnectionObject;