﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChatUI.Models
{
    public class ChatModel
    {
        public String UserName { get; set; }
        public String PhoneNumber {get;set;}
        public String ChannelId { get; set; }
    }
}
