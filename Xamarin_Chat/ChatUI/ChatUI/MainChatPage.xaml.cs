﻿using ChatUI.Models;
using ChatUI.ViewModels;
using MvvmHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using Quobject.SocketIoClientDotNet.Client;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ChatUI
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MainChatPage : ContentPage
	{
        MainChatViewModel vm;
        
        public MainChatPage(Plugin.ContactService.Shared.Contact content, Socket Socket, ObservableRangeCollection<Message> MessagesList,bool flag)
        {
            InitializeComponent();
           
            BindingContext = vm = new MainChatViewModel(this, content, Socket, MessagesList, flag);


            vm.Messages.CollectionChanged += (sender, e) =>
            {
                var target = vm.Messages[vm.Messages.Count - 1];
                MessagesListView.ScrollTo(target, ScrollToPosition.End, true);
            };

            BindingContext = vm;
        }

        void MyListView_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            MessagesListView.SelectedItem = null;
        }

        void MyListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            MessagesListView.SelectedItem = null;
        }
    }
}