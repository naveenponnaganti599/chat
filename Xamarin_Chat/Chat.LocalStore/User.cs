﻿namespace Chat.LocalStore
{
    public class User : BaseTable
    {
        public string UserName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Primary { get; set; }
        public string ChannelType { get; set; }
        public string ChannelId { get; set; }
    }
}
