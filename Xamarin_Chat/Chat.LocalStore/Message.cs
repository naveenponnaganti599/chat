﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chat.LocalStore
{
    public class Message : BaseTable
    {
        public string MessageText { get; set; }
        public string ChannelId { get; set; }
        public ChatUser SentByPrimary { get; set; }
       // public string SentByUserId { get; set; }
        public bool IsInComming { get; set; }
        public DateTime time { get; set; }
        public bool IsSeen { get; set; }
    }
}
