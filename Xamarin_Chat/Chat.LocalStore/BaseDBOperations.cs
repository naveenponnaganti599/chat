﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Chat.LocalStore
{
    public class BaseDBOperations
    {
        private SQLiteConnection _connection;
        private readonly string _dbPath;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbPath">Local database file path</param>
        /// <param name="deleteDatabase">Action to invoke for deleting database</param>
        public BaseDBOperations(string dbPath)
        {
            try
            {
                _dbPath = dbPath;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception: " + ex.Message);
                throw;
            }
        }

        public SQLiteConnection DatabaseObject()
        {
            return new SQLiteConnection(_dbPath);
        }

        /// <summary>
        /// This will invoke the DeleteDatabase method in native applciation which is platform specfic
        /// </summary>
        public void DeleteDatabase()
        {
            try
            {
                //_deleteDatabase.Invoke();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception: " + ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Creates a Table based on the 'T' type
        /// </summary>
        /// <returns></returns>
        public void CreateTable<T>() where T : class
        {
            try
            {
                using (_connection = new SQLiteConnection(_dbPath))
                {
                    _connection.CreateTable<T>();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception: " + ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Delete a Table based on the 'T' type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public void DropTable<T>() where T : class
        {
            try
            {
                using (_connection = new SQLiteConnection(_dbPath))
                {
                    _connection.DropTable<T>();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception: " + ex.Message);
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>All the rows of a table</returns>
        public List<T> Get<T>() where T : class, new()
        {
            try
            {
                using (_connection = new SQLiteConnection(_dbPath))
                {
                    return _connection.Table<T>().ToList();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception: " + ex.Message);
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <returns>Single row with Provided ID as primary key</returns>
        public List<T> Get<T>(string id) where T : class, new()
        {
            try
            {
                using (_connection = new SQLiteConnection(_dbPath))
                {
                    return _connection.Table<T>().ToList();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception: " + ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This will insert or update a list of records
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj">Single record to save</param>
        public void Save<T>(List<T> obj) where T : class
        {
            try
            {
                using (_connection = new SQLiteConnection(_dbPath))
                {
                    _connection.InsertAll(obj);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception: " + ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This will insert/update a record
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj">Multiple records to save</param>
        public void Save<T>(T obj) where T : class
        {
            try
            {
                using (_connection = new SQLiteConnection(_dbPath))
                {
                    _connection.Insert(obj);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception: " + ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This will permanently remove a record from database
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj">Single record to delete permanently</param>
        public void PermanentDelete<T>(T obj) where T : BaseTable
        {
            try
            {
                using (_connection = new SQLiteConnection(_dbPath))
                {
                    _connection.Delete(obj);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception: " + ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This will permanently remove all records from database
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public void PermanentDelete<T>() where T : BaseTable
        {
            try
            {
                using (_connection = new SQLiteConnection(_dbPath))
                {
                    _connection.DeleteAll<T>();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception: " + ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This is a soft delete operation for single record
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj">Single record to soft delete</param>
        //public void Delete<T>(T obj) where T : BaseTable
        //{
        //    try
        //    {
        //        obj.IsDeleted = true;
        //        using (_connection = new SQLiteConnection(_dbPath))
        //        {
        //            _connection.Update(obj);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Debug.WriteLine("Exception: " + ex.Message);
        //        throw;
        //    }
        //}

        /// <summary>
        /// This is a soft delete operation for multiple records
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj">Multiple records to soft delete</param>
        //public void Delete<T>(List<T> obj) where T : BaseTable
        //{
        //    try
        //    {
        //        obj.Select(c => { c.IsDeleted = true; return c; }).ToList();
        //        using (_connection = new SQLiteConnection(_dbPath))
        //        {
        //            _connection.UpdateAll(obj);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Debug.WriteLine("Exception: " + ex.Message);
        //        throw;
        //    }
        //}
    }
}
