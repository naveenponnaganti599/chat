﻿using System;
using System.Collections.Generic;

namespace Chat.LocalStore
{
    public class Channel : BaseTable
    {
        public string ChannelType { get; set; }
        public DateTime ChannelCreatedAt { get; set; }
        public string _id { get; set; }
        public List<ChatUser> ChatUsers { get; set; }
        public bool IsAdmin { get; set; }
    }
}
